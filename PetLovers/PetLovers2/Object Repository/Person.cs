﻿
namespace Agl.PetLovers.Objects
{
    public class Person
    {
        public string name { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        public Pet[] pets { get; set; }        
    }
}
