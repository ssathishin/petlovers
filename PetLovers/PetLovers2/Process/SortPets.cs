﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net;
using System.Configuration;
using Agl.PetLovers.Objects;

namespace Agl.PetLovers.Process
{
    public class SortPets
    {
        private List<Person> people;
        private readonly string[] genders = { "male", "female" };
        
        public string GetPetInfo()
        {
            WebClient webClient = new WebClient();
            string rawJSON = webClient.DownloadString(ConfigurationManager.AppSettings["PeopleServiceURI"]);
            people = JsonConvert.DeserializeObject<List<Person>>(rawJSON);

            //Remove people from the list who don't own pets to prevent looping through
            for (int i = 0; i < people.Count; i++)
            {
                if (people[i].pets == null)
                {
                    people.RemoveAt(i);
                }
            }
            return rawJSON;
        }

        public Dictionary<string, List<string>> SortPetsByNameByOwnerGender(string petType)
        {
            Dictionary<string, List<string>> petsSortedByGender = new Dictionary<string, List<string>>();
            try
            {
                Console.WriteLine("\n");

                foreach (string ownerGender in genders)
                {
                    List<string> petsList = new List<string>();
                    foreach (Person person in people)
                    {
                        foreach (Pet pet in person.pets)
                        {
                            if (pet.type.ToLower().Equals(petType.ToLower()) && person.gender.ToLower().Equals(ownerGender.ToLower()))
                                petsList.Add(pet.name);
                        }
                    }

                    petsList.Sort();
                    petsSortedByGender.Add(ownerGender, petsList);
                    

                    if (petsList.Count.Equals(0))
                    {
                        Console.WriteLine("Poor {0} no {1} owner owns it. :(", petType, ownerGender);
                    }
                    else
                    {
                        Console.WriteLine("Pet(s) owned by {1} owners sorted by {0} names \n", petType, ownerGender);
                        foreach (string pet in petsList)
                        {
                            Console.WriteLine(pet);
                        }
                    }

                    Console.WriteLine("");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return petsSortedByGender;
        }

        public void DisplaySortedPetsOnScreen(Dictionary<string,List<string>> sortedPetsListByGender)
        {
            foreach(string gender in genders)
            {
                
            }
        }
    }
}
