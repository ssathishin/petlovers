﻿using System;
using Agl.PetLovers.Process;

namespace Agl.PetLovers
{
    class Program
    {
        static void Main(string[] args)
        {
            SortPets petSorter = new SortPets();
            petSorter.GetPetInfo();

            Console.WriteLine("Please enter one of the pet types from the following list (cat, dog, fish) : ");
            petSorter.SortPetsByNameByOwnerGender(Console.ReadLine().ToString());

            Console.WriteLine("\n");
            Console.WriteLine("Press Enter key to exit");
            Console.Read();
        }
    }
}
