﻿Feature: PetSorter
	As a pet lover
	I would like to see the pet names sorted by their owner's gender	

@Positive
Scenario Outline: Display sorted pet names by their owner gender
	Given I have information about people and their pets
	When I request for pet names by type '<Type>'
	Then I should see pet names sorted alphabetically and grouped by their owner's gender
	Examples: 
	| Type |
	| Cat  |
	| Dog  |
	| Fish |

@Negative
Scenario: Display error message if the requested pet is not found
	Given I have information about people and their pets
	When I request for pet names by type 'rabbit'
	Then I should see an error message that no one owns the requested pet type