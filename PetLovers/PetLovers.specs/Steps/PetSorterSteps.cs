﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Agl.PetLovers.Process;
using NUnit.Framework;


namespace PetLovers.specs.Steps
{
    [Binding]
    public sealed class PetSorterSteps
    {
        SortPets petSorter = new SortPets();
        Dictionary<string, List<string>> sortedPetsList = new Dictionary<string, List<string>>();
        string type;
        string[] maleOwnerCats = { "Garfield", "Jim", "Max", "Tom" };
        string[] femaleOwnerCats = { "Garfield", "Simba", "Tabby" };
        string[] maleOwnerDogs = { "Fido", "Sam" };
        string[] femaleOwnerDogs = { };
        string[] maleOwnerFishes = { };
        string[] femaleOwnerFishes = { "Nemo" };
        string[] petsNotFound = { };

        [Given(@"I have information about people and their pets")]
        public void GivenIHaveInformationAboutPeopleAndTheirPets()
        {
            string rawJson = "";
            rawJson = petSorter.GetPetInfo();
            Console.WriteLine(rawJson);
        }

        [When(@"I request for pet names by type '(.*)'")]
        public void WhenIRequestForPetNamesByType(string petType)
        {
            sortedPetsList = petSorter.SortPetsByNameByOwnerGender(petType);
            type = petType;
        }

        [Then(@"I should see an error message that no one owns the requested pet type")]        
        [Then(@"I should see pet names sorted alphabetically and grouped by their owner's gender")]
        public void ThenIShouldSeePetNamesSortedAlphabeticallyAndGroupedByTheirOwnerSGender()
        {
            string[] genders = { "male", "female" };
            switch (type.ToLower())
            {
                case "dog":
                    VerifyPetCount(maleOwnerDogs, femaleOwnerDogs);
                    VerifyPetNamesSortOrder("male", maleOwnerDogs);
                    VerifyPetNamesSortOrder("female", femaleOwnerDogs);                
                    break;
                case "cat":
                    VerifyPetCount(maleOwnerCats, femaleOwnerCats);
                    VerifyPetNamesSortOrder("male", maleOwnerCats);
                    VerifyPetNamesSortOrder("female", femaleOwnerCats);
                    break;
                case "fish":
                    VerifyPetCount(maleOwnerFishes, femaleOwnerFishes);
                    VerifyPetNamesSortOrder("male", maleOwnerFishes);
                    VerifyPetNamesSortOrder("female", femaleOwnerFishes);
                    break;
                default:
                    VerifyPetCount(petsNotFound,petsNotFound);
                    Console.WriteLine("Poor {0} no one owns it. :( ",type);
                    Assert.False(type.Equals("cat") || type.Equals("dog") || type.Equals("fish"));
                    break;

            }
        }

        public void VerifyPetNamesSortOrder(string ownerGender, string[] petNames)
        {
            int i = 0;
            foreach(string petName in petNames)
            {
                Assert.True(petName.ToLower().Equals(sortedPetsList[ownerGender][i].ToLower()));
                Console.WriteLine("Expected pet name is: {0}; Actual pet name is: {1}", petName, sortedPetsList[ownerGender][i]);
                i++;
            }               
        }

        public void VerifyPetCount(string[] maleOwnerPets, string[] femaleOwnerPets)
        {
            Assert.True(maleOwnerPets.Count().Equals(sortedPetsList["male"].Count()) && femaleOwnerPets.Count().Equals(sortedPetsList["female"].Count()));            
        }
    }
}
