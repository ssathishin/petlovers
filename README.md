***Pet Lovers***

**Requirements**

http://agl-developer-test.azurewebsites.net/

**Technology**

1. C#
2. .Net 4.5.2
3. Visual Studio 2015 Community Edition

**Third party libraries**

1. Newtonsoft.Json version 11.0.2
2. Specflow 2.3.0
3. Specrun.Runner 1.7.1
4. Specrun.Specflow 1.7.1

**Version Control**

Git

**Repository url:**

https://bitbucket.org/ssathishin/petlovers

---

## Projects

**1. PetLovers**
**2. PetLovers.Specs**


## PetLovers:

**SortPets object provides two behaviours:**

1. GetPetInfo()
2. SortPetsByNameByOwnerGender(string petType)


**GetPetInfo():**

	1. Downloads JSON message from http://agl-developer-test.azurewebsites.net/people.json
	2. Deserializes the downloaded JSON message to List<Person>.
	3. Removes people from the list those who don't own pets.

**SortPetsByNameByOwnerGender(string petType):**
	
	1. Takes petType parameter and loops through the List<Person> for pet type by owner's gender
	2. Sorts pet names alphabetically by owner's gender.
	3. If pet type does not exists in the downloaded message then it displays "Poor pet type no one owns it"
	4. If pet type does exists in the downloaded message then the pet name is printed alphabetically by its owner's gender.


## PetLovers.Specs:

This is the unit test project built to ensure that behaviour implemented in PetLovers project is functioning as intended.

There are 3 positive tests and 1 negative test in PetSorter.feature file.

Each positive test case in the suite: 

1. Creates an instance of SortPets object 
2. Invokes GetPetInfo function to download and serialize JSON object 
3. Invokes SortPetsByNameByOwnerGender function by supplying pet type and stores the sorted pets list.
4. Asserts that the requested pet type information is returned in alphabetically sorted order.

Negative test case :

Calls steps 1 to 3 from the positive test suite and then Asserts that the sorted pets list returned by SortPetsByNameByOwnerGender function doesn't contain any pet information. 